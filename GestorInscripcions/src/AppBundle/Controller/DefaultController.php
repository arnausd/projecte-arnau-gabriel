<?php

	namespace AppBundle\Controller;

	use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
	use Symfony\Bundle\FrameworkBundle\Controller\Controller;
	use Symfony\Component\HttpFoundation\Request;
	use Symfony\Component\Form\Extension\Core\Type\TextType;
	use Symfony\Component\Form\Extension\Core\Type\DateType;
	use Symfony\Component\Form\Extension\Core\Type\IntegerType;
	use Symfony\Component\Form\Extension\Core\Type\SubmitType;
	use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
	use Symfony\Bridge\Doctrine\Form\Type\EntityType;

	use AppBundle\Entity\Persona;
	use AppBundle\Entity\Esdeveniment;
	use AppBundle\Entity\Inscripcio;
	use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
	/**
	*@Route("/", name="homepage")
	*/
	
	public function indexAction(Request $request) {
		
		return $this->render('base.html.twig', array(
			'message' => null, 'form'=> null));
    }
    
	/**
	*@Route("/creapersonaauto")
	*/
	
	public function createAction() {
		
		$persona = new Persona;
		
		$persona->setNom("Gabriel");
		$persona->setCognom1("Camina");
		$persona->setCognom2("Rodríguez");
		$persona->setNif("41016434m");
		$persona->setNumLlicencia(1);
		$persona->setTipusLlicencia("Majors habilitada - C");
		
		$em = $this->getDoctrine()->getManager();
		$em->persist($persona);
		if(!$em->flush()) {			//retorna false si va ser exitos
			return new Response('Created persona id ' .$persona->getId());
		}
		else {
			return new Response('NO s\'ha creat!');
		}
		
					
    }
    
    
    /**
	*@Route("/crearPersona")
	*/
    
    
    public function newAction(Request $request) {

        $persona = new Persona();
     
        $form = $this->createFormBuilder($persona)
            ->add('nom', TextType::class)
            ->add('cognom1', TextType::class)
            ->add('cognom2', TextType::class)
            ->add('nif', TextType::class)
            ->add('numLlicencia', IntegerType::class)
            ->add('tipusLlicencia', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create persona'))
            ->getForm();
		
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {

				$em = $this->getDoctrine()->getManager();
				$em->persist($persona);
				if(!$em->flush()) {			//retorna false si va ser exitos
			//		return new Response('Created persona id ' .$persona->getId());
					return $this->redirect('http://192.168.201.158:8000/mostrarPersones');
				}
				
				return new Response('No s\'ha pogut crear a la persona');
			}

			return $this->render('default/new.html.twig', array(
          'form' => $form->createView(),
			
			
        ));
    }
    
	/**
	*@Route("/crearEsdeveniment")
	*/

    public function newAction2(Request $request) {
        
        $esdeveniment = new  Esdeveniment();
        
        $form = $this->createFormBuilder($esdeveniment)
			->add('nom', TextType::class)
            ->add('ubicacio', TextType::class)
            ->add('maxParticipants', IntegerType::class)
            ->add('data', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Crear esdeveniment'))
            ->getForm();

		//esto mete los datos,o quizas no, quien sabe
		
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {

				$em = $this->getDoctrine()->getManager();
				$em->persist($esdeveniment);
				if(!$em->flush()) {			//retorna false si va ser exitos
					return new Response('Nou esdeveniment id ' .$esdeveniment->getId());
				}
				
				return new Response('No s\'ha pogut crear l\'esdeveniment');
			}

        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    
    /**
	*@Route("/inscripcio")
	*/

    public function newAction4(Request $request) {

        $inscripcio = new  Inscripcio();
        
        $form = $this->createFormBuilder($inscripcio)
        
            ->add('nomPersona', TextType::class)
            ->add('cognomPersona', TextType::class)
            ->add('cognom2Persona', TextType::class)
            ->add('nif', TextType::class)
            ->add('nomEsdeveniment', TextType::class)
            ->add('dataNaixement', DateType::class)
            ->add('save', SubmitType::class, array('label' => 'Inscriure'))
            ->getForm();

		
		$form->handleRequest($request);
		
		if ($form->isSubmitted() && $form->isValid()) {

				$em = $this->getDoctrine()->getManager();
				$em->persist($inscripcio);
				if(!$em->flush()) {			//retorna false si va ser exitos
					return new Response('Nova inscripció id ' .$inscripcio->getId());
				}
				
				return new Response('No s\'ha pogut crear l\'inscripció');
			}

        return $this->render('default/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }
    
    /**
	*@Route("/mostrarEsdeveniments")
	*/

    public function selectAll(Request $request) {

		$esdeveniments = $this->getDoctrine()
			->getRepository('AppBundle:Esdeveniment')
			->findAll();

		if(count($esdeveniments)==0) {
			return $this->render('default/error.html.twig' , array(
			'missatge' => 'No hi ha esdeveniments'));
		}
		else{
			return $this->render('Esdeveniments/content.html.twig', array(
			'esdeveniments' => $esdeveniments));
		}
    } 
     
    /**
	*@Route("/mostrarPersones")
	*/

    public function selectAllPersones(Request $request) {

		$persones = $this->getDoctrine()
			->getRepository('AppBundle:Persona')
			->findAll();

		if(count($persones)==0) {
			return $this->render('default/error.html.twig' , array(
			'missatge' => 'No hi ha persones'));
		}
		else{
			return $this->render('Persona/content.html.twig', array(
			'persones' => $persones));
		}
    } 
        
}
