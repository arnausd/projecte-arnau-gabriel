<?php
	
	// src/AppBundle/Entity/Persona.php

	namespace AppBundle\Entity;
	
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity
	 * @ORM\Table(name="persona")
	*/

	
	class Persona {

		/**
		 * @ORM\Column(type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
	    protected $id;
	    
	     /**
		 * @ORM\Column(type="string", columnDefinition="CHAR(11)")
		 */
	    protected $nif;
	    
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $nom;
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $cognom1;
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $cognom2;
	    
	    /**
		 * @ORM\Column(type="integer")
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
	    protected $numLlicencia;
	    
	    /**
		 * @ORM\Column(type="string", length=20)
		 */
	    protected $tipusLlicencia;
	
	      	
	 /**
     * @ORM\ManyToMany(targetEntity="Esdeveniment", inversedBy="persona")
     * @ORM\JoinTable(name="llistaEsdeveniments")
     */
     
     protected $esdeveniment;
	    
	    
	    
	  //GETTERS  
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nif
     *
     * @param string $nif
     *
     * @return Persona
     */
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif
     *
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Persona
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set cognom1
     *
     * @param string $cognom1
     *
     * @return Persona
     */
    public function setCognom1($cognom1)
    {
        $this->cognom1 = $cognom1;

        return $this;
    }

    /**
     * Get cognom1
     *
     * @return string
     */
    public function getCognom1()
    {
        return $this->cognom1;
    }

    /**
     * Set cognom2
     *
     * @param string $cognom2
     *
     * @return Persona
     */
    public function setCognom2($cognom2)
    {
        $this->cognom2 = $cognom2;

        return $this;
    }

    /**
     * Get cognom2
     *
     * @return string
     */
    public function getCognom2()
    {
        return $this->cognom2;
    }

    /**
     * Set numLlicencia
     *
     * @param integer $numLlicencia
     *
     * @return Persona
     */
    public function setNumLlicencia($numLlicencia)
    {
        $this->numLlicencia = $numLlicencia;

        return $this;
    }

    /**
     * Get numLlicencia
     *
     * @return integer
     */
    public function getNumLlicencia()
    {
        return $this->numLlicencia;
    }

    /**
     * Set tipusLlicencia
     *
     * @param string $tipusLlicencia
     *
     * @return Persona
     */
    public function setTipusLlicencia($tipusLlicencia)
    {
        $this->tipusLlicencia = $tipusLlicencia;

        return $this;
    }

    /**
     * Get tipusLlicencia
     *
     * @return string
     */
    public function getTipusLlicencia()
    {
        return $this->tipusLlicencia;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->esdeveniment = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add esdeveniment
     *
     * @param \AppBundle\Entity\Esdeveniment $esdeveniment
     *
     * @return Persona
     */
    public function addEsdeveniment(\AppBundle\Entity\Esdeveniment $esdeveniment)
    {
        $this->esdeveniment[] = $esdeveniment;

        return $this;
    }

    /**
     * Remove esdeveniment
     *
     * @param \AppBundle\Entity\Esdeveniment $esdeveniment
     */
    public function removeEsdeveniment(\AppBundle\Entity\Esdeveniment $esdeveniment)
    {
        $this->esdeveniment->removeElement($esdeveniment);
    }

    /**
     * Get esdeveniment
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEsdeveniment()
    {
        return $this->esdeveniment;
    }
}
