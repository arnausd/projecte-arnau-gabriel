<?php
	
	// src/AppBundle/Entity/Inscripcio.php

	namespace AppBundle\Entity;
	
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity
	 * @ORM\Table(name="inscripcio")
	*/

	
	class Inscripcio {

		/**
		 * @ORM\Column(type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
	    protected $id;
	    
	     /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $nomEsdeveniment;    
	    
	     /**
		 * @ORM\ManyToMany(targetEntity="Esdeveniment", mappedBy="inscripcio")
		 */
	    protected $idEsdeveniment;    
	    	    
	     /**
		 * @ORM\Column(type="string", columnDefinition="CHAR(11)")
		 */
	    protected $nif;
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $nomPersona;    
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $cognomPersona;    	    
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $cognom2Persona;    
	    
	    
	    /**
		 * @ORM\Column(type="date")
		 */
	    protected $dataNaixement;    


	    protected $opciones;     
	    
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomEsdeveniment
     *
     * @param string $nomEsdeveniment
     *
     * @return Inscripcio
     */
    public function setNomEsdeveniment($nomEsdeveniment)
    {
        $this->nomEsdeveniment = $nomEsdeveniment;

        return $this;
    }

    /**
     * Get nomEsdeveniment
     *
     * @return string
     */
    public function getNomEsdeveniment()
    {
        return $this->nomEsdeveniment;
    }

    /**
     * Set nif
     *
     * @param string $nif
     *
     * @return Inscripcio
     */
    public function setNif($nif)
    {
        $this->nif = $nif;

        return $this;
    }

    /**
     * Get nif
     *
     * @return string
     */
    public function getNif()
    {
        return $this->nif;
    }

    /**
     * Set nomPersona
     *
     * @param string $nomPersona
     *
     * @return Inscripcio
     */
    public function setNomPersona($nomPersona)
    {
        $this->nomPersona = $nomPersona;

        return $this;
    }

    /**
     * Get nomPersona
     *
     * @return string
     */
    public function getNomPersona()
    {
        return $this->nomPersona;
    }

    /**
     * Set cognomPersona
     *
     * @param string $cognomPersona
     *
     * @return Inscripcio
     */
    public function setCognomPersona($cognomPersona)
    {
        $this->cognomPersona = $cognomPersona;

        return $this;
    }

    /**
     * Get cognomPersona
     *
     * @return string
     */
    public function getCognomPersona()
    {
        return $this->cognomPersona;
    }

    /**
     * Set cognom2Persona
     *
     * @param string $cognom2Persona
     *
     * @return Inscripcio
     */
    public function setCognom2Persona($cognom2Persona)
    {
        $this->cognom2Persona = $cognom2Persona;

        return $this;
    }

    /**
     * Get cognom2Persona
     *
     * @return string
     */
    public function getCognom2Persona()
    {
        return $this->cognom2Persona;
    }

    /**
     * Set dataNaixement
     *
     * @param \DateTime $dataNaixement
     *
     * @return Inscripcio
     */
    public function setDataNaixement($dataNaixement)
    {
        $this->dataNaixement = $dataNaixement;

        return $this;
    }

    /**
     * Get dataNaixement
     *
     * @return \DateTime
     */
    public function getDataNaixement()
    {
        return $this->dataNaixement;
    }
}
