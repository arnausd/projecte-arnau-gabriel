<?php

	// src/AppBundle/Entity/Esdeveniment.php

	namespace AppBundle\Entity;
	
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity
	 * @ORM\Table(name="esdeveniments")
	*/
	
	 
	class Esdeveniment {
		
		/**
		 * @ORM\Column(type="integer")
		 * @ORM\Id
		 * @ORM\GeneratedValue(strategy="AUTO")
		 */
	    protected $id;
	    
		/**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $nom;
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $ubicacio;
	    
	    /**
		 * @ORM\Column(type="string", length=30)
		 */
	    protected $maxParticipants;
	    
	    /**
		 * @ORM\Column(type="date")
		 */
	    protected $data;
		 
		
		/**
		 * @ORM\ManyToMany(targetEntity="Persona", mappedBy="esdeveniment")
		*/
		
		protected $persona; 
		 
		/**
		 * @ORM\ManyToMany(targetEntity="Inscripcio", inversedBy="idEsdeveniment")
		 * @ORM\JoinTable(name="esdeveniments_id")
		 */
		 
		 protected $inscripcio;
		 
		 
		 //GETTERS
		
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Esdeveniment
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set ubicacio
     *
     * @param string $ubicacio
     *
     * @return Esdeveniment
     */
    public function setUbicacio($ubicacio)
    {
        $this->ubicacio = $ubicacio;

        return $this;
    }

    /**
     * Get ubicacio
     *
     * @return string
     */
    public function getUbicacio()
    {
        return $this->ubicacio;
    }

    /**
     * Set maxParticipants
     *
     * @param string $maxParticipants
     *
     * @return Esdeveniment
     */
    public function setMaxParticipants($maxParticipants)
    {
        $this->maxParticipants = $maxParticipants;

        return $this;
    }

    /**
     * Get maxParticipants
     *
     * @return string
     */
    public function getMaxParticipants()
    {
        return $this->maxParticipants;
    }

    /**
     * Set data
     *
     * @param \DateTime $data
     *
     * @return Esdeveniment
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \DateTime
     */
    public function getData()
    {
        return $this->data;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->persona = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add persona
     *
     * @param \AppBundle\Entity\Persona $persona
     *
     * @return Esdeveniment
     */
    public function addPersona(\AppBundle\Entity\Persona $persona)
    {
        $this->persona[] = $persona;

        return $this;
    }

    /**
     * Remove persona
     *
     * @param \AppBundle\Entity\Persona $persona
     */
    public function removePersona(\AppBundle\Entity\Persona $persona)
    {
        $this->persona->removeElement($persona);
    }

    /**
     * Get persona
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPersona()
    {
        return $this->persona;
    }
}
